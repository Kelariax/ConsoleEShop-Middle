﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Middle
{
    public interface IAuthorization
    {
        public string Login { get; }
        public string Password { get; set; }

        public bool PasswordVerification(string password);
    }
}
