﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Middle
{
    public abstract class UserMenu
    {
        public DataBase DataBase { get; set; }
        public Account Account { get; set; }

        public Dictionary<int, Action> Functions { get; set; }


        protected UserMenu ()
        {
            Functions = new Dictionary<int, Action> {
                {1, this.OutputGoods },
                {2, this.FindGood },
            };
        }
        protected UserMenu(DataBase dataBase, Account account)
            :this()
        {
            DataBase = dataBase;
            Account = account;
        }


        public abstract void OutputUserFunctions();
        public void OutputGoods()
        {
            int count = 0;
            int page = 1;
            Console.WriteLine($"<<Goods list>><<Page: {page}>>");
            foreach (var item in DataBase.goodsDB)
            {
                Console.WriteLine(item + "\n");
                count++;
                if (count % 5 == 0)
                {
                    Console.WriteLine("Any key - Continue\nEsc - Stop");
                    if (!ContinueCheck())
                    {
                        break;
                    }
                    Console.Clear();
                    page++;
                    Console.WriteLine($"<<Goods list>><<Page: {page}>>");
                }
            }
            Console.WriteLine("Press any key...");
            Console.ReadKey();
        }
        public void FindGood()
        {
            do
            {
                Good good;
                Console.WriteLine("<<Good search>>");
                Console.Write("Enter good name: ");
                good = DataBase.goodsDB.Find(Console.ReadLine());
                if (good == null)
                {
                    Console.WriteLine("There is no good with that name!");
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine(good);
                }
                Console.WriteLine("\nAny key - Continue\nEsc - Stop");
                if (!ContinueCheck())
                {
                    Console.Clear();
                    break;
                }
                Console.Clear();
            } while (true);    
        }
        public bool ContinueCheck()
        {
            var symb = Console.ReadKey();
            if (symb.Key == ConsoleKey.Escape)
            {
                return false;
            }
            return true;
        }

        public void ExecuteCommand(int command)
        {
            if(Functions.ContainsKey(command))
            {
                Functions[command].Invoke();
            }
            else
            {
                Console.WriteLine("\nThere is no such command");
                Console.WriteLine("\nPress any key...");
                Console.ReadKey();
            }
        }
    }
}
