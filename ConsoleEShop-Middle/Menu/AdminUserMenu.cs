﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Middle
{
    class AdminUserMenu : RegisteredUserMenu
    {
        public AdminUserMenu(DataBase dataBase, Account account)
            : base(dataBase, account)
        {
        }
        public override void OutputUserFunctions() => Console.WriteLine("<<Console E-Shop>><<User>>\n1 - Show all goods\n2 - Search good by name\n3 - Make new order\n" +
            "4 - Confirm order\n5 - Change user's info\n6 - Add new good\n7 - Change good info\n8 - Change order status\n9 - Log out");
        public void ChangeUserInfo()
        {
            Account account = null;
            do
            {
                if (account != null)
                {
                    EditPersonalInfo(account);
                    account = null;
                    Console.WriteLine("\nAny key - Continue with another user\nEsc - Stop");
                    if (!ContinueCheck())
                    {
                        Console.Clear();
                        break;
                    }
                }
                else
                {
                    Console.WriteLine("<<Editing user's info>>");
                    Console.WriteLine("\nEnter user's login: ");
                    string userLogin = Console.ReadLine();
                    int index;
                    index = DataBase.accountsDB.IndexOf(userLogin);
                    if (index < 0)
                    {
                        Console.WriteLine("\nThere is no such user!");
                        Console.WriteLine("\nAny key - Continue\nEsc - Stop");
                        if (!ContinueCheck())
                        {
                            Console.Clear();
                            break;
                        }
                    }
                    else
                    {
                        account = DataBase.accountsDB[index];
                    }
                }
                Console.Clear();
            } while (true);
        } 

        public void CreateNewGood()
        {
            Good good = new Good();
            int command;
            do
            {
                Console.WriteLine("<<Good creation>>");
                Console.WriteLine(good);
                Console.WriteLine("\n1 - Edit good's field\n2 - Add good\n3 - Back");
                Console.Write("Choose command: ");
                if (!int.TryParse(Console.ReadLine(), out command))
                {
                    Console.WriteLine("\nWrong input value!");
                    Console.WriteLine("\nAny key - Continue\nEsc - Stop");
                    if (!ContinueCheck())
                    {
                        Console.Clear();
                        break;
                    }
                }
                else
                {


                    switch (command)
                    {
                        case 1:
                            Console.Clear();
                            EditGood(good);
                            break;
                        case 2:

                            if (good.Name != "Empty" && good.Category != "Empty" && good.Price != 0)
                            {
                                DataBase.goodsDB.Add(good);
                                Console.Clear();

                                Console.WriteLine("<<Good creation>>");
                                Console.WriteLine(good);
                                Console.WriteLine($"\nGoods successfully added!");
                                Console.WriteLine("\nPress any key...");
                                Console.ReadKey();
                                return;
                            }
                            else
                            {
                                Console.WriteLine("\nFields name, category, price can't be empty!");
                                Console.WriteLine("Press any key...");
                                Console.ReadKey();
                            }
                            break;
                        case 3:
                            return;
                        default:
                            Console.WriteLine("There is no such command!");
                            Console.ReadKey();
                            break;
                    }
                }
                Console.Clear();
            } while (true);
        }
        public void ChangeGoodInfo()
        {
            Good good = null;
            do
            {
                if (good != null)
                {
                    Console.Clear();
                    EditGood(good);
                    good = null;
                    Console.WriteLine("\nAny key - Continue with another good\nEsc - Stop");
                    if (!ContinueCheck())
                    {
                        Console.Clear();
                        break;
                    }
                }
                else
                {
                    Console.WriteLine("<<Editing good's info>>");
                    Console.WriteLine("\nEnter good's name: ");
                    string goodName = Console.ReadLine();
                    int index;
                    index = DataBase.goodsDB.IndexOf(goodName);
                    if (index < 0)
                    {
                        Console.WriteLine("\nThere is no such good!");
                        Console.WriteLine("\nAny key - Continue\nEsc - Stop");
                        if (!ContinueCheck())
                        {
                            Console.Clear();
                            break;
                        }
                    }
                    else
                    {
                        good = DataBase.goodsDB[index];
                    }
                }
                Console.Clear();
            } while (true);
        }
        private void EditGood(Good good)
        {
            int command;
            do
            {
                Console.WriteLine("<<Good editing>>");
                Console.WriteLine(good);
                Console.WriteLine("\n1 - Name\n2 - Category\n3 - Description\n4 - Price\n5 - Back");
                Console.Write("Choose command: ");
                if (!int.TryParse(Console.ReadLine(), out command))
                {
                    Console.WriteLine("\nWrong input value!");
                    Console.WriteLine("\nAny key - Continue\nEsc - Stop");
                    if (!ContinueCheck())
                    {
                        Console.Clear();
                        break;
                    }
                }
                else if (command == 4)
                {
                    Console.Clear();
                    Console.WriteLine("<<Good editing>>");
                    Console.WriteLine(good);

                    Console.Write("\nEnter new price: ");
                    decimal price;

                    if (!decimal.TryParse(Console.ReadLine(), out price) || price <= 0)
                    {
                        Console.WriteLine("\nWrong input value!");
                        Console.WriteLine("\nAny key - Continue\nEsc - Stop");
                        if (!ContinueCheck())
                        {
                            Console.Clear();
                            break;
                        }
                    }
                    else
                    {

                        good.Price = price;
                        Console.Clear();

                        Console.WriteLine("<<Good editing>>");
                        Console.WriteLine(good);
                        Console.WriteLine($"\nField price successfully changed!");
                        Console.WriteLine("\nPress any key...");
                        Console.ReadKey();
                    }

                }
                else if (command == 1 || command == 2 || command == 3)
                {
                    Console.Clear();

                    Console.WriteLine("<<Good editing>>");
                    Console.WriteLine(good);
                    string commandStr = string.Empty;
                    switch (command)
                    {
                        case 1:
                            commandStr = "name";
                            break;
                        case 2:
                            commandStr = "category";
                            break;
                        case 3:
                            commandStr = "description";
                            break;
                    }
                    Console.Write($"\nEnter new {commandStr}: ");
                    string value = Console.ReadLine();
                    if (value == string.Empty)
                    {
                        Console.WriteLine("\nWrong string value!");
                        Console.WriteLine("\nAny key - Continue\nEsc - Stop");
                        if (!ContinueCheck())
                        {
                            Console.Clear();
                            break;
                        }
                    }
                    else
                    {
                        switch (command)
                        {
                            case 1:
                                good.Name = value;
                                break;
                            case 2:
                                good.Category = value;
                                break;
                            case 3:
                                good.Description = value;
                                break;
                        }
                        Console.Clear();

                        Console.WriteLine("<<Good editing>>");
                        Console.WriteLine(good);
                        Console.WriteLine($"\nField {commandStr} successfully changed!");
                        Console.WriteLine("\nPress any key...");
                    }
                }
                else if (command == 5)
                {
                    break;
                }
                Console.Clear();
            } while (true);
        }
        public void ChangeOrderStatus()
        {
            Order order = null;
            do
            {
                if (order != null)
                {
                    if (!order.IsConfirmed || order.Status == OrderStatus.CanceledByUser || order.Status == OrderStatus.Completed)
                    {
                        Console.WriteLine("<<Changing order status>>");
                        Console.WriteLine(order);
                        Console.WriteLine("\nYou can't change order status in cases:\n1. Order is not confirmed\n2. Order canceled by user\n3. Order completed");
                        order = null;
                        Console.WriteLine("Press any key...");
                        Console.ReadKey();
                    }
                    else
                    {
                        Console.Clear();

                        ChangeStatus(order);
                        Console.Clear();
                        Console.WriteLine("<<Changing order status>>");
                        Console.WriteLine(order);
                        Console.WriteLine("\nOrder status successfully changed!");
                        order = null;
                        Console.WriteLine("\nAny key - Continue with another order\nEsc - Stop");
                        if (!ContinueCheck())
                        {
                            Console.Clear();
                            break;
                        }
                    }
                }
                else
                {
                    Console.WriteLine("<<Changing order status>>");
                    Console.WriteLine("\nEnter order's id: ");
                    int orderID;
                    if (!int.TryParse(Console.ReadLine(), out orderID))
                    {
                        Console.WriteLine("\nWrong input value!");
                        Console.WriteLine("\nAny key - Continue\nEsc - Stop");
                        if (!ContinueCheck())
                        {
                            Console.Clear();
                            break;
                        }
                    }
                    else
                    {
                        int index;
                        index = DataBase.orderDB.IndexOf(orderID);
                        if (index < 0)
                        {
                            Console.WriteLine("\nThere is no such order!");
                            Console.WriteLine("\nAny key - Continue\nEsc - Stop");
                            if (!ContinueCheck())
                            {
                                Console.Clear();
                                break;
                            }
                        }
                        else
                        {
                            order = DataBase.orderDB[index];
                        }
                    }
                }
                Console.Clear();
            } while (true);
        }

        private void ChangeStatus(Order order)
        {
            int command;
            do
            {
                Console.WriteLine("<<Changing order status>>");
                Console.WriteLine(order);

                Console.WriteLine("\n1 - Canceled by admin\n2 - Sent\n3 - Received payment\n4 - Completed");
                Console.Write("Choose status: ");
                if (!int.TryParse(Console.ReadLine(), out command))
                {
                    Console.WriteLine("\nWrong input value!");
                    Console.WriteLine("\nAny key - Continue\nEsc - Stop");
                    if (!ContinueCheck())
                    {
                        Console.Clear();
                        break;
                    }
                }
                else
                {
                    switch (command)
                    {
                        case 1:
                            order.Status = OrderStatus.CanceledByAdmin;
                            return;
                        case 2:
                            order.Status = OrderStatus.Sent;
                            return;
                        case 3:
                            order.Status = OrderStatus.ReceivedPayment;
                            return;
                        case 4:
                            order.Status = OrderStatus.Completed;
                            return;
                        default:
                            Console.WriteLine("\nThere is no such status");
                            Console.WriteLine("\nPress any key...");
                            break;
                    }
                }
                Console.Clear();
            } while (true);
        }
    }
}
