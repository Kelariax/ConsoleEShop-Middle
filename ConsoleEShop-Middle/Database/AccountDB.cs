﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Middle
{
    public class AccountDB : IEnumerable<Account>, IDatabase<Account, string>
    {
        public List<Account> Accounts { get; private set; }
        public AccountDB()
        {
            Accounts = new List<Account>();
        }
        public AccountDB(Account account)
            : this()
        {
            Accounts.Add(account);
        }
        public AccountDB(IEnumerable<Account> accounts)
        {
            Accounts = new List<Account>(accounts);
        }

        public Account this[int index]
        {
            get
            {
                if (index >= 0 && index < Accounts.Count)
                {
                    return Accounts[index];
                }
                throw new ArgumentOutOfRangeException(new string("Index is out of range"));
            }
        }
        public void Add(Account item)
        {
            if(item == null)
            {
                throw new ArgumentNullException(nameof(item));
            }
            if (Accounts.Contains(item))
            {
                throw new ArgumentException(nameof(item), new string("Account with same login already exists in DB"));
            }
            Accounts.Add(item);
        }
        public Account Find(string login)
        {
            int index = IndexOf(login);
            if (index < 0)
            {
                return null;
            }
            return this[index];

        }
        public int IndexOf(string login)
        {
            int count = 0;
            if(this.Contains(login))
            {
                foreach (var item in Accounts)
                {
                    if (item.Login == login)
                    {
                        return count;
                    }
                    count++;
                }
            }
            return -1;
        }
        public bool Contains(Account item)
        {
            foreach (Account account in Accounts)
            {
                if ((account).Equals(item))
                {
                    return true;
                }
            }
            return false;
        }
        public bool Contains(string login)
        {
            foreach (Account item in Accounts)
            {
                if (item.Login == login)
                {
                    return true;
                }
            }
            return false;
        }
      
        public IEnumerator<Account> GetEnumerator()
        {
            return Accounts.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return Accounts.GetEnumerator();
        }

       
    }
}
