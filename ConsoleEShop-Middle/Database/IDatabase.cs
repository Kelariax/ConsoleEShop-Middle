﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Middle
{
    public interface IDatabase <T, in K>
    {
        public void Add(T item);

        public T Find(K name);

        public int IndexOf(K name);

        public bool Contains(T item);

        public bool Contains(K name);
    }
}
