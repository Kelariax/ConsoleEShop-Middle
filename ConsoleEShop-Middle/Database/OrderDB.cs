﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Middle
{
    public class OrderDB : IDatabase <Order, int>
    {
        public List<Order> Orders { get; private set; }
        public OrderDB()
        {
            Orders = new List<Order>();
        }
        public OrderDB(Order order)
            : this()
        {
            Orders.Add(order);
        }
        public OrderDB(IEnumerable<Order> orders)
        {
            Orders = new List<Order>(orders);
        }

        public Order this[int index]
        {
            get
            {
                if (index >= 0 && index < Orders.Count)
                {
                    return Orders[index];
                }
                throw new ArgumentOutOfRangeException(new string("Index is out of range"));
            }
        }
        public Order Find(int id)
        {
            int index = IndexOf(id);
            if (index < 0)
            {
                return null;
            }
            return this[index];
        }

        public int IndexOf(int id)
        {
            int count = 0;
            if (this.Contains(id))
            {
                foreach (var item in Orders)
                {
                    if (item.Id == id)
                    {
                        return count;
                    }
                    count++;
                }
            }
            return -1;
        }
        public void Add(Order item)
        {
            if(item == null)
            {
                throw new ArgumentNullException(new string("Order is null"));
            }
            if(this.Contains(item.Id))
            {
                throw new ArgumentException(new string("Order with such id already exists"));
            }
            Orders.Add(item);
        }
        public bool Contains(Order item)
        {
            foreach (Order order in Orders)
            {
                if (order.Equals(item))
                {
                    return true;
                }
            }
            return false;
        }
        public bool Contains(int id)
        {
            return Orders.Exists(i => i.Id == id);
        }
    }
}
