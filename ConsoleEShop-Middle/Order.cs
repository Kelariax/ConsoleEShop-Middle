﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Middle
{
    public enum OrderStatus
    {
        New, CanceledByAdmin, CanceledByUser, Sent, Received, ReceivedPayment, Completed,        
    }
    public class Order
    {
        public int UserId { get; }
        public int Id { get; }

        public DateTime OrderTime{ get; }
        public static int Count { get;  private set; }
        public OrderStatus Status { get; set; }

        public bool IsConfirmed { get; set; }
        public List<OrderItem> OrderItems { get; set; }

        static Order()
        {
            Count = 0;
        }
        public Order(Good good, int userId, int goodQuantity)
        {
            Count++;
            Id = Count;
            UserId = userId;
            OrderItems = new List<OrderItem>();
            OrderItems.Add(new OrderItem(good, goodQuantity));
            IsConfirmed = false;
            Status = OrderStatus.New;
            OrderTime = DateTime.Now;
        }


        public void Add(OrderItem orderItem)
        {
            if (orderItem == null || orderItem.Good == null)
            {
                throw new ArgumentNullException(nameof(orderItem));
            }
            foreach (var item in OrderItems)
            {
                if(item.Good.Equals(orderItem.Good))
                {
                    item.GoodQuantity += orderItem.GoodQuantity;
                    return;
                }
            }
            OrderItems.Add(orderItem);
        }
        
        public override string ToString()
        {
            string itemsStr = "";
            decimal sum = 0;
            foreach (var item in OrderItems)
            {
                itemsStr += item.ToString() + "\n";
                sum += item.Good.Price * item.GoodQuantity;
            }
            return new string($"Order Id: {Id}\nDate: {OrderTime.ToShortDateString()} || Order status: {Status} || Is confirmed: {IsConfirmed}\nList of goods:\n{itemsStr}Total price: {Math.Round(sum, 2)}");
        }
    }
}
