﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Middle
{

    class EShopController
    {
        public Account account;
        public DataBase dataBase;

        public EShopController(DataBase dataBase)
        {
            this.dataBase = dataBase;
        }
        public EShopController(IEnumerable<Good> goods, IEnumerable<Account> accounts, IEnumerable<Order> orders)
        {
            this.dataBase = new DataBase(goods, accounts, orders);
        }
        public void Run()
        {
            account = null;
            UserMenu userMenu = null;

            int command;
            while (true)
            {
                command = 0;
                try
                {
                    if (account == null)
                    {
                        userMenu = new GuestMenu(dataBase, account);
                        ((GuestMenu)userMenu).LogInNotify += LogInHandler;
                        ((GuestMenu)userMenu).RegisterUserNotify += RegisterUserHandler;
                        userMenu.OutputUserFunctions();

                        Console.Write("Choose command: ");

                        command = Convert.ToInt32(Console.ReadLine());

                        Console.Clear();

                        userMenu.ExecuteCommand(command);
                    }
                    else
                    {
                        
                        switch (account.Permision)
                        {
                            case PermisionStatus.RegisteredUser:

                                userMenu = new SimpleUserMenu(dataBase, account);
                                ((RegisteredUserMenu)userMenu).LogOutNotify += LogOutHandler;
                                ((SimpleUserMenu)userMenu).OutputUserFunctions();
                                Console.Write("Choose command: ");
                                command = Convert.ToInt32(Console.ReadLine());
                                Console.Clear();

                                userMenu.ExecuteCommand(command);

                                break;
                            case PermisionStatus.Admin:
                                userMenu = new AdminUserMenu(dataBase, account);
                                ((RegisteredUserMenu)userMenu).LogOutNotify += LogOutHandler;
                                ((AdminUserMenu)userMenu).OutputUserFunctions();
                                Console.Write("Choose command: ");
                                command = Convert.ToInt32(Console.ReadLine());
                                Console.Clear();

                                userMenu.ExecuteCommand(command);
                                break;
                        }
                    }


                }
                catch (Exception ex)
                {
                    Console.Clear();
                    Console.WriteLine(ex.Message);
                    Console.WriteLine("\nAny key - Continue\nEsc - Stop");
                    if (!userMenu.ContinueCheck())
                    {
                        break;
                    }
                }
                Console.Clear();
            }

        }

        public void LogInHandler(object sender, EventArgs e)
        {
            account = sender as Account;
        }

        public void RegisterUserHandler(object sender, EventArgs e)
        {
            account = sender as Account;
        }

        public void LogOutHandler(object sender, EventArgs e)
        {
            account = sender as Account;
        }
    }
}
