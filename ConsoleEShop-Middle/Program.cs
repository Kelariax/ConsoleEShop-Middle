﻿using System;
using System.Collections.Generic;

namespace ConsoleEShop_Middle
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Good> goods = new List<Good>() { new Good("T-Shirt", "Clothing", "T-Shirt", Convert.ToDecimal(15.99)), new Good("Baseball Cap", "Clothing", "Cap", Convert.ToDecimal(9.99)) };
            List<Account> accounts = new List<Account>() { new Account("User", "Password"), new Account("admin", "admin", PermisionStatus.Admin) };
            List<Order> orders = new List<Order>();
            EShopController eShopController = new EShopController(new DataBase(goods, accounts, orders));
            eShopController.Run();

        }
    }
}
